package ru.t1.amsmirnov.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.ServerAboutRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.ServerVersionRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.ServerAboutResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest serverAboutRequest);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest serverVersionRequest);

}
