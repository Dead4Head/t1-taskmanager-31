package ru.t1.amsmirnov.taskmanager.api.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public interface ICommand {

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @Nullable
    String getName();

    @Nullable
    Role[] getRoles();

    void execute() throws AbstractException;

}
