package ru.t1.amsmirnov.taskmanager.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.Domain;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.system.TransportFileException;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public final class DataXmlSaveFasterXMLCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-xml";

    @NotNull
    public static final String DESCRIPTION = "Save data to XML file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        try {
            System.out.println("[DATA SAVE XML]");
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_XML);
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(xml.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (final Exception exception) {
            throw new TransportFileException(FILE_XML, exception);
        }
    }

}
