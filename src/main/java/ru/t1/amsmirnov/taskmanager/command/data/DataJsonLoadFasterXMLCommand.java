package ru.t1.amsmirnov.taskmanager.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.Domain;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.system.TransportFileException;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataJsonLoadFasterXMLCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json";

    @NotNull
    public static final String DESCRIPTION = "Load data from JSON file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        try {
            System.out.println("[DATA LOAD JSON]");
            @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_JSON));
            @NotNull final String json = new String(bytes);
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
            setDomain(domain);
        } catch (final Exception exception) {
            throw new TransportFileException(FILE_JSON, exception);
        }
    }

}
