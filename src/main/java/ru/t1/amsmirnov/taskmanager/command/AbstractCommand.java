package ru.t1.amsmirnov.taskmanager.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.model.ICommand;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @NotNull
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String description = getDescription();
        final String argument = getArgument();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

    @Nullable
    public String getUserId() throws AbstractException {
        return serviceLocator.getAuthService().getUserId();
    }

}
