package ru.t1.amsmirnov.taskmanager.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.endpoint.Operation;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResponse;
import ru.t1.amsmirnov.taskmanager.task.AbstractServerTask;
import ru.t1.amsmirnov.taskmanager.task.ServerAcceptTask;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class Server {

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();

    @NotNull
    private final Bootstrap bootstrap;

    @Nullable
    private ServerSocket serverSocket;

    public Server(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void submit(@NotNull final AbstractServerTask task) {
        executorService.execute(task);
    }

    public void start() {
        @NotNull final IPropertyService propertyService = bootstrap.getPropertyService();
        @NotNull final Integer port = propertyService.getServerPort();
        try {
            serverSocket = new ServerSocket(port);
        } catch (@NotNull IOException e) {
            bootstrap.getLoggerService().error(e);
        }
        submit(new ServerAcceptTask(this));
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass,
            @NotNull final Operation<RQ, RS> operation
    ) {
        dispatcher.registry(reqClass, operation);
    }

    @NotNull
    public Object call(@NotNull final AbstractRequest request) {
        return dispatcher.call(request);
    }

    public void stop() {
        if (serverSocket == null) return;
        executorService.shutdown();
        try {
            serverSocket.close();
        } catch (@NotNull IOException e) {
            bootstrap.getLoggerService().error(e);
        }
    }

    @Nullable
    public ServerSocket getServerSocket() {
        return this.serverSocket;
    }

    public IServiceLocator getServiceLocator() {
        return this.bootstrap;
    }

}
