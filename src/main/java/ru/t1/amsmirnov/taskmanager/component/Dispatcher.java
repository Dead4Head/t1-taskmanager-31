package ru.t1.amsmirnov.taskmanager.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.endpoint.Operation;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResponse;

import java.util.LinkedHashMap;
import java.util.Map;

public final class Dispatcher {

    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull Class<RQ> reqClass,
            @NotNull Operation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    @NotNull
    public Object call(@NotNull AbstractRequest request) {
        @Nullable final Operation operation = map.get(request.getClass());
        return operation.execute(request);
    }

}
