package ru.t1.amsmirnov.taskmanager.dto.response;

import org.jetbrains.annotations.Nullable;

public class ServerAboutResponse extends AbstractResponse{

    @Nullable
    private String email;

    @Nullable
    private String name;

    @Nullable
    public String getEmail() {
        return this.email;
    }

    public void setEmail(@Nullable final String email) {
        this.email = email;
    }

    @Nullable
    public String getName() {
        return this.name;
    }

    public void setName(@Nullable final String name) {
        this.name = name;
    }

}
