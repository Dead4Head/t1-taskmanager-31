package ru.t1.amsmirnov.taskmanager.exception.user;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public final class LoginExistException extends AbstractException {

    public LoginExistException() {
        super("Error! This login is already used...");
    }

}
