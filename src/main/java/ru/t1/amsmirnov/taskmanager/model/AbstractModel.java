package ru.t1.amsmirnov.taskmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

public abstract class AbstractModel implements Serializable {

    @Getter
    @Setter
    @NotNull
    protected String id = UUID.randomUUID().toString();

}
