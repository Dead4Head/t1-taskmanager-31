package ru.t1.amsmirnov.taskmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractUserOwnedModel extends AbstractModel {

    @Getter
    @Setter
    @NotNull
    private String userId;

}
