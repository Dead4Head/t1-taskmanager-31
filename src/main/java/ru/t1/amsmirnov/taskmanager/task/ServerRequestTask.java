package ru.t1.amsmirnov.taskmanager.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.component.Server;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractRequest;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public class ServerRequestTask extends AbstractServerSocketTask {

    public ServerRequestTask(
            @NotNull final Server server,
            @NotNull final Socket socket
    ) {
        super(server, socket);
    }

    @Override
    public void run() {
        try {
            @NotNull final InputStream inputStream = socket.getInputStream();
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            @NotNull final Object object = objectInputStream.readObject();
            @NotNull final AbstractRequest request = (AbstractRequest) object;
            @Nullable final Object response = server.call(request);
            @NotNull final OutputStream outputStream = socket.getOutputStream();
            @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(response);
            server.submit(new ServerRequestTask(server, socket));
        } catch (final Exception exception) {
            server.getServiceLocator().getLoggerService().error(exception);
        }
    }

}
